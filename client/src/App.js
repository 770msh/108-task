import axios from 'axios';
import { React, useState, useEffect } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Text from './component/text';
import Visual from './component/visual';


const App = () => {

  // Defines 3 hooks that will contain the values of the object
  const [altitude, set_altitude] = useState([]);
  const [hsi, set_his] = useState([]);
  const [adi, set_adi] = useState([]);

  // Activates the doApi once
  useEffect(() => {
    doApi();
  }, [])

  // Gets the object from the server and puts the values into the hooks 
  // By axios
  const doApi = async () => {
    let url = "http://localhost:3001/";

    await axios.get(url)
      .then((res) => {
        set_altitude(res.data.altitude);
        set_his(res.data.hsi);
        set_adi(res.data.adi);
        //console.log(res.data);
      })
  }

  //Design elements  
  const divStyle = {
    display: "flex",
    width: "800px",
    padding: "8px",
    margin: "0 auto",
    marginBottom: "25px"


  }
  const btnStyle = {
    marginLeft: "10px",
    margin: "auto",
    padding: "15px",
    borderRadius: "5px",
  }
  const link = {
    textDecoration: "none"
  }

  //Defines links to pages
  //Defines the Routes of the pages
  //Sends the data to components
  
  return (
    <BrowserRouter>
      <div style={divStyle}>
        <button style={btnStyle}><Link style={link} to="/" >VISUAL</Link></button>
        <br />
        <button style={btnStyle}><Link style={link} to="/text" >TEXT</Link></button>
      </div>
      <Routes>
        <Route path="/" element={<Visual altitude={altitude} adi={adi} hsi={hsi} />} />
        <Route path="/text" element={<Text altitude={altitude} adi={adi} hsi={hsi} />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;