import React from 'react'

export default function Text(props) {

  //Design element
  const divStyle = {
    width: "800px",
    padding: "8px",
    margin: "0 auto",
    backgroundColor: "gray",
    minHeight: "500px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  }

  //Displays the data on the page
  return (
    <div style={divStyle}>
      <h2>Altitude : {props.altitude}</h2>
      <h2>Hsi : {props.hsi}</h2>
      <h2>Adi : {props.adi}</h2>
    </div>
  )
}
