import React from 'react'

export default function HsiComp(props) {

  //Design elements
  const divStyle = {
    height: "200px",
    width: "200px",
    backgroundColor: "white",
    borderRadius: "50%",
    transform: "rotate(" + (-props.hsi) + "deg)",
    transition: "transform 3.5s"
  }

  const arrow = {
    position: "absolute",
    fontSize: '40px',
    right: "820px",
    top: "325px"
  }

  const lineXup = {
    textAlign: "center",
    lineHeight: "25px"
  }

  const lineX = {
    lineHeight: "120px",
    wordSpacing: "144px"
  }
  const lineY = {
    textAlign: "center",
    lineHeight: "1px"
  }


  return (
    <div>
      <div style={divStyle}>
        <h3 style={lineXup}>0</h3>
        <h3 style={lineX}>270 90</h3>
        <h3 style={lineY}>180</h3>
      </div>
      <div style={arrow}>&#8593;</div>
    </div>
  )
}
