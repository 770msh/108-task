import React from 'react'
import HsiComp from './hsiComp';
import AltitudeComp from './altitudeComp'
import AdiComp from './adiComp'

export default function Visual(props) {

  //Design elements
  const divStyle = {
    width: "800px",
    padding: "8px",
    margin: "0 auto",
    backgroundColor: "gray",
    minHeight: "500px",
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center"
  }

  //Sends the data to components
  return (
    <div style={divStyle}>
      <AltitudeComp altitude={props.altitude} />
      <HsiComp hsi={props.hsi} />
      <AdiComp adi={props.adi} />
    </div>
  )
}
