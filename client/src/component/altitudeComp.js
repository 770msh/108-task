import React from 'react';


export default function AltitudeComp(props) {

  //Design elements
  const styleDiv = {
    height: "360px",
    width: "75px",
    backgroundColor: "white",
    borderRadius: "12px",

  }
  const value = {
    height: "300px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
  const line = {
    top: 500 - (props.altitude / 10),
    height: "5px",
    width: "75px",
    border: "5px solid",
    borderRadius: "45px",
    position: "absolute",
    transition: "top 3.5s"
  }

  const styleText = {
    marginTop: 60
  }


  return (
    <div>
      <div style={styleDiv}>
        <div style={value}>
          <h3>3000</h3>
          <h3 style={styleText}>2000</h3>
          <h3 style={styleText}>1000</h3>
          <h3 style={styleText} >0</h3>
          <div style={line}></div>
        </div>
      </div>
    </div>
  )
}
