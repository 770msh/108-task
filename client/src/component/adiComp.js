import React, { useEffect, useState } from 'react'

export default function AdiComps(props) {

  //Setting Variables
  const [blue, setBlue] = useState();
  const [green, setGreen] = useState();
  let adi = props.adi;
  let absAdi = Math.abs(props.adi)


  //Calculation of percentages
  useEffect(() => {
    if (adi < 0) {
      setGreen(absAdi * 3.6);
      setBlue(360 - green);
    } else if (adi > 0) {
      setBlue(adi * 3.6);
      setGreen((360 - blue));
    } else {
      setBlue(180);
      setGreen(180);
    }

  });


  //Design elements
  const style = {
    height: "200px",
    width: "200px",
    backgroundColor: "white",
    borderRadius: "50%",
    backgroundImage: 'conic-gradient(rgb(80, 182, 246) 0deg,rgb(80, 182, 246)' + blue + 'deg , rgb(50, 248, 110) 0deg, rgb(50, 248, 110)' + green + 'deg)'
  }

  return (
    <div style={style}>
    </div>
  )
}
