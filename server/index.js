const express = require("express");
const router = express.Router();
const { input, data } = require("./inputVal");


// Receives information from the user
// inputVal is executed in the file
input(data);

//get request
router.get("/", (req, res) => {
    res.send(data);
})

module.exports = router;