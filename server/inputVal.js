//get input from user by prompt module
const prompt = require('prompt-sync')({ sigint: true });
const pressAnyKey = require('press-any-key');

//create object
//Default the values to 0 
exports.data = {
    altitude: 0,
    hsi: 0,
    adi: 0
};

//validate the input 
exports.input = (data) => {
    let altitude = Number(prompt("enter a value between 0-3000 "));
    while (altitude < 0 || altitude > 3000) {
        altitude = Number(prompt("enter a value between 0-3000 "));
    }
    data.altitude = altitude;
    let hsi = Number(prompt("enter a value between 0-360 "));
    while (hsi < 0 || hsi > 360) {
        hsi = Number(prompt("enter a value between 0-360 "));
    }
    data.hsi = hsi;
    let adi = Number(prompt("enter a value: between -100 - 100 "));
    while (adi < -100 || adi > 100) {
        adi = Number(prompt("enter a value: between -100 - 100  "));
    }
    data.adi = adi;

    //press any key
    pressAnyKey("press any key\n")
        .then(() => {
            console.log('data send')
        })
};




