// import method
const express = require("express");
const app = express();
const http = require("http");
const router = express.Router();
const indexR = require("./index");


// * -> Indicates that a request can be made from any domain
app.all('*', function (req, res, next) {
    if (!req.get('Origin')) return next();
    res.set('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.set('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,x-api-key');
    next();
});

// Sends get request from index file
app.use(indexR);
 

// Generate server and address
const server = http.createServer(app);
const port = process.env.Port || "3001";
// The server is listening to the address we selected
server.listen(port);

















// const cors = require("cors");
// const {Server} = require("socket.io");
// app.use(cors());





// const io = new Server(server,{
//   cors: {
//     origin: true
  
//   }
// });


// io.on("connection", (socket) =>{
//   console.log(`user connect: ${socket.id}`);
//   socket.send(data);

//   // socket.on("send_data" ,(data) => {
//   //   socket.send(data);
//   // })
  
  
//   io.on("disconnect", ()=>{
//     console.log("socket disconnect");
//   })
// })




  